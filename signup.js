let signupFormElement = document.getElementById("signupForm")


let firstNameElement = document.getElementById("firstName");
let firstNameErrMsgElement = document.getElementById("firstNameErrMsg");

let lastNameElement = document.getElementById("lastName")
let lastNameErrMsgElement = document.getElementById("lastNameErrMsg")

let emailElement = document.getElementById("email");
let emailErrMsgElement = document.getElementById("emailErrMsg");

let passwordElement = document.getElementById("password")
let reEnterPasswordElement = document.getElementById("reEnterPassword")
let passwordErrMsgElement = document.getElementById("passwordErrMsg")
let reEnterPasswordErrMsgElement = document.getElementById("reEnterPasswordErrMsg")
let passwordMisMatchErrMsgElement = document.getElementById("passwordMisMatchErrMsg")

let tosCheckboxElement = document.getElementById("tosCheckbox")
let tosErrMsgElement = document.getElementById("tosErrMsg")

let congratsMsgElement = document.getElementById("congratsMsg")
let redirectionLinkElement = document.getElementById('redirectionLink')

let submissionErrMsgElement = document.getElementById("submissionErrMsg")

let submitButtonElement = document.getElementById("submitButton");

// console.log(submitButtonElement)
let hasNumber = /\d/;
let format = /[!@#$%^&*()_+\-=\[\]{};~`':"\\|,.<>\/?]+/;
const reg = /^(([^<>()[\]\\.,:\s@"]+(\.[^<>()[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g;
    

firstNameElement.addEventListener('blur', function (event) {
    console.log("im in blur event of firstname")

    if (firstNameElement.value === "") {
        firstNameErrMsgElement.textContent = "Enter a valid firstname without spaces, numbers, special charecters*"
    } else if (firstNameElement.value.includes(" ")) {
        firstNameErrMsgElement.textContent = "Enter a valid firstname without spaces, numbers, special charecters*"
    } else if (hasNumber.test(event.target.value) || format.test(event.target.value)) {
        firstNameErrMsgElement.textContent = "Enter a valid firstname without spaces, numbers, special charecters*"
    } else {
        firstNameErrMsgElement.textContent = "";
    }


})




lastNameElement.addEventListener('blur', function (event) {
    console.log("im in blur event of lastname")
    let hasNumber = /\d/;
    let format = /[!@#$%^&*()_+\-=\[\]{};~`':"\\|,.<>\/?]+/;
    if (lastNameElement.value === "") {
        lastNameErrMsgElement.textContent = "Enter a valid lastname without spaces, numbers, special charecters*"
    } else if (lastNameElement.value.includes(" ")) {
        lastNameErrMsgElement.textContent = "Enter a valid lastname without spaces, numbers, special charecters*"
    } else if (hasNumber.test(event.target.value) || format.test(event.target.value)) {
        lastNameErrMsgElement.textContent = "Enter a valid lastname without spaces, numbers, special charecters*"
    } else {
        lastNameErrMsgElement.textContent = "";
    }
});

emailElement.addEventListener('blur', function (event) {
    console.log('inside blur event of email')
    //console.log(reg.test(emailElement.value));
    if (emailElement.value === "") {
        emailErrMsgElement.textContent = "E-mail cannot be empty*";
    } else if (reg.test(emailElement.value)) {
        emailErrMsgElement.textContent = "";
    } else {
        emailErrMsgElement.textContent = "Enter a valid E-mail*";
    }


});

passwordElement.addEventListener('blur', function (event) {
    console.log(event.target.value.length)
    if (passwordElement.value === "") {
        passwordErrMsgElement.textContent = "Password length should atleast be 6*";
    } else if (event.target.value.length < 6) {
        passwordErrMsgElement.textContent = "Password length should atleast be 6";
    } else {
        passwordErrMsgElement.textContent = "";
    }
})

reEnterPasswordElement.addEventListener('blur', function (event) {

    if (reEnterPasswordElement.value === "") {
        reEnterPasswordErrMsgElement.textContent = "Password length should atleast be 6*";
    } else {
        reEnterPasswordErrMsgElement.textContent = "";
        if (event.target.value === passwordElement.value) {
            passwordMisMatchErrMsgElement.textContent = "";
        } else {
            passwordMisMatchErrMsgElement.textContent = "Passwords Mismatched*";
        }
    }


})



submitButtonElement.addEventListener('click', function (event) {
    event.preventDefault()

    console.log("im in click event")
    let hasNumber = /\d/;
    let format = /[!@#$%^&*()_+\-=\[\]{};~`':"\\|,.<>\/?]+/;


    if (firstNameElement.value === "") {
        firstNameErrMsgElement.textContent = "Enter a valid firstname without spaces, numbers, special charecters*"
    } else if (firstNameElement.value.includes(" ")) {
        firstNameErrMsgElement.textContent = "Enter a valid firstname without spaces, numbers, special charecters*"
    } else if (hasNumber.test(firstNameElement.value) || format.test(firstNameElement.value)) {
        firstNameErrMsgElement.textContent = "Enter a valid firstname without spaces, numbers, special charecters*"
    } else {
        firstNameErrMsgElement.textContent = "";
    }

    if (lastNameElement.value === "") {
        lastNameErrMsgElement.textContent = "Enter a valid lastname without spaces, numbers, special charecters*"
    } else if (lastNameElement.value.includes(" ")) {
        lastNameErrMsgElement.textContent = "Enter a valid lastname without spaces, numbers, special charecters*"
    } else if (hasNumber.test(lastNameElement.value) || format.test(lastNameElement.value)) {
        lastNameErrMsgElement.textContent = "Enter a valid lastname without spaces, numbers, special charecters*"
    } else {
        lastNameErrMsgElement.textContent = "";
    }

    if (emailElement.value === "") {
        emailErrMsgElement.textContent = "E-mail cannot be empty sub*";
    } 
    
    // else if (reg.test(emailElement.value)) {
    //     emailErrMsgElement.textContent = "";
    // } else {
    //     emailErrMsgElement.textContent = "Enter a valid E-mail sub*";
    // }

    if (passwordElement.value === "") {
        passwordErrMsgElement.textContent = "Password length should atleast be 6*";
    } else if (passwordElement.value.length < 6) {
        passwordErrMsgElement.textContent = "Password length should atleast be 6";
    } else {
        passwordErrMsgElement.textContent = "";
    }

    if (reEnterPasswordElement.value === "") {
        reEnterPasswordErrMsgElement.textContent = "Password length should atleast be 6*";
        passwordMisMatchErrMsgElement.textContent = "";
    } else {
        reEnterPasswordErrMsgElement.textContent = "";
        if (reEnterPasswordElement.value === passwordElement.value) {
            passwordMisMatchErrMsgElement.textContent = "";
        } else {
            passwordMisMatchErrMsgElement.textContent = "Passwords Mismatched*";
        }
    }

    if (tosCheckboxElement.checked === false) {
        tosErrMsgElement.textContent = "Please accept the terms to create your account*"
    } else {
        tosErrMsgElement.textContent = "";

    }

    let passwordsMatched = passwordElement.textContent === reEnterPasswordElement.textContent
    let correctFirstName = firstNameErrMsgElement.textContent.length === 0;
    let correctLastName = lastNameErrMsgElement.textContent.length === 0;
    console.log(emailErrMsgElement.textContent)
    let correctEmail = emailErrMsgElement.textContent ==="";
    console.log(passwordsMatched, correctFirstName, correctLastName, correctEmail)

    if ((correctFirstName && correctLastName && correctEmail && passwordElement.value.length >= 6 && reEnterPasswordElement.value.length >= 6 && passwordsMatched && tosCheckboxElement.checked)) {
        // congratsMsgElement.textContent = "Congrats your new Mailchimp account has been created."
        tosErrMsgElement.textContent = ""
        congratsMsgElement.textContent = `congrats ${firstNameElement.value} ${lastNameElement.value} your account has been created successfully`;
        redirectionLinkElement.textContent = `Navigate to products dashboard by clicking here`
        // firstNameElement.value = "";
        // lastNameElement.value = "";
        // emailElement.value = "";
        // passwordElement.value = "";
        // reEnterPasswordElement.value = "";
        // tosCheckboxElement.checked = false;

        // submissionErrMsgElement.textContent = ""
    } else {
        congratsMsgElement.textContent = ""
        redirectionLinkElement.textConten = ""
        // submissionErrMsgElement.textContent = "please fill required feilds"


    }
});


