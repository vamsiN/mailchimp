let productsContainerElement = document.getElementById('productsContainer');
productsContainerElement.classList.add("products-container")
let spinnerEl=document.getElementById('spinner')
let apiErrMsgElement = document.getElementById("apiErrMsg")




function createAndAppendProductsCard(obj){
    let {category, description, id, image, price, rating, title} = obj;

    let cardContainer = document.createElement('div')
    cardContainer.classList.add("card-container");

    // cardImageContainer
    let cardImageContainer = document.createElement("div")
    cardImageContainer.classList.add("card-image-container")
    
    let cardImage = document.createElement('img')
    cardImage.src = image;
    cardImage.classList.add("card-image");
    cardImageContainer.appendChild(cardImage)

    // cardDetailsContainer
    

    let cardDetailsContainer = document.createElement('div');
    cardDetailsContainer.classList.add('card-details-container')

    let cardTitle = document.createElement("p")
    cardTitle.textContent = title;
    cardTitle.classList.add('card-title')
    cardDetailsContainer.append(cardTitle)

    let cardCategory = document.createElement('p')
    cardCategory.textContent = category.toUpperCase();
    cardCategory.classList.add('card-category')
    cardDetailsContainer.append(cardCategory)
    
    let cardDescription = document.createElement("p")
    cardDescription.textContent = description;
    cardDescription.classList.add('card-description')
    cardDetailsContainer.append(cardDescription)

    

    let cardPrice = document.createElement('p')
    cardPrice.textContent = `$ ${price}`
    cardPrice.classList.add('card-price')
    cardDetailsContainer.append(cardPrice)

    let caredRating = document.createElement('p') 
    caredRating.textContent =  `Rating: ${rating.rate} (${rating.count})`;
    caredRating.classList.add('card-rating')
    cardDetailsContainer.append(caredRating)

    //appending to cardContainer
    cardContainer.appendChild(cardImageContainer)
    cardContainer.appendChild(cardDetailsContainer)

    productsContainerElement.appendChild(cardContainer)
}

function displayResults(productsArray){
    
    productsContainerElement.removeChild(spinnerEl)
    
    productsArray.forEach((product) => {
        console.log('creating and appending products')
        createAndAppendProductsCard(product)
    });
}



fetch("https://fakestoreapi.com/products/")
    .then((response) => {
        console.log('inside fetch')
        return response.json()
    })
    .then((json) => {
        if(json.length ===0 ){
            apiErrMsgElement.textContent = "Sorry, No products to show presently"
        }else{
            apiErrMsgElement.textContent = "";
            displayResults(json);
        }
        
    }).catch((error)=>{
        productsContainerElement.removeChild(spinnerEl)
        apiErrMsgElement.textContent = "Unable to fectch the results"
    })